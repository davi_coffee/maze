const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     WGW W",
    "W W W WWW WWWWW W W W",
    "W WCW   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "WG        W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W WC  W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W WCW",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
const maze = document.getElementById('maze')
const positionPerson = {
    'row': undefined,
    'column': undefined
}

const positionGhost = {
    'row': [],
    'column': []
}

const positionCherry = []
let contCherry = 0

const createDivs = {
    'W': function (i, j) {
        let wall = document.createElement('div')
        wall.className = 'box wall'
        wall.id = `matriz${i}-${j}`
        maze.appendChild(wall)
    },
    ' ': function (i, j) {
        let way = document.createElement('div')
        way.className = 'box way'
        way.id = `matriz${i}-${j}`

        maze.appendChild(way)
    },
    'S': function (i, j) {
        let person = document.createElement('img')
        person.id = 'person'
        person.src = 'pac.png'

        positionPerson.row = i
        positionPerson.column = j

        let start = document.createElement('div')
        start.className = 'box start'
        start.id = `matriz${i}-${j}`

        start.appendChild(person)
        maze.appendChild(start)
    },
    'F': function (i, j) {
        let finish = document.createElement('div')
        finish.className = 'box finish'
        finish.id = `matriz${i}-${j}`

        maze.appendChild(finish)
    },
    'G': function (i, j) {
        let ghost = document.createElement('img')
        ghost.id = `ghost${i}`
        ghost.src = 'pacred.png'

        positionGhost.row.push(i)
        positionGhost.column.push(j)

        let way = document.createElement('div')
        way.className = 'box way'
        way.id = `matriz${i}-${j}`

        way.appendChild(ghost)
        maze.appendChild(way)
    },
    'C': function (i, j) {
        let cherry = document.createElement('img')
        cherry.id = `cherry${i}`
        cherry.src = 'fruit.png'

        positionCherry.push([i, j])

        let way = document.createElement('div')
        way.className = 'box way'
        way.id = `matriz${i}-${j}`

        way.appendChild(cherry)
        maze.appendChild(way)
    }
}


const movePerson = {
    'ArrowRight': function (person) {
        let nextWay = document.getElementById(`matriz${positionPerson.row}-${positionPerson.column + 1}`)

        checkLooser(nextWay)
        if (nextWay && (nextWay.classList.contains('way') || nextWay.classList.contains('finish'))) {
            person.className = 'slideRight'
            nextWay.append(person)
            positionPerson.column++
            checkCherry()
        }
    },
    'ArrowLeft': function (person) {
        let nextWay = document.getElementById(`matriz${positionPerson.row}-${positionPerson.column - 1}`)
        checkLooser(nextWay)

        if (nextWay && (nextWay.classList.contains('way') || nextWay.classList.contains('finish'))) {
            person.className = 'slideLeft'
            nextWay.append(person)
            positionPerson.column--
        }
        checkCherry()
    },
    'ArrowUp': function (person) {
        let nextWay = document.getElementById(`matriz${positionPerson.row - 1}-${positionPerson.column}`)
        checkLooser(nextWay)

        if (nextWay && (nextWay.classList.contains('way') || nextWay.classList.contains('finish'))) {
            person.className = 'slideUp'
            nextWay.append(person)
            positionPerson.row--
        }
        checkCherry()
    },
    'ArrowDown': function (person) {
        let nextWay = document.getElementById(`matriz${positionPerson.row + 1}-${positionPerson.column}`)
        checkLooser(nextWay)

        if (nextWay && (nextWay.classList.contains('way') || nextWay.classList.contains('finish'))) {
            person.className = 'slideDown'
            nextWay.append(person)
            positionPerson.row++
        }
        checkCherry()
    }
}

for (let i = 0; i < map.length; i++) {
    for (let j = 0; j < map[i].length; j++) {
        createDivs[map[i][j]](i, j)
    }
}

document.body.addEventListener('keydown', listenerKeyboard)

function listenerKeyboard(event) {
    let person = document.getElementById('person')
    let key = event.key

    if (movePerson[key]) {
        movePerson[key](person)
    }
    checkWinner(person)
}

// GHOST
function ghostVertical() {
    let ghost = document.getElementById('ghost5')
    let index = 0

    CallgrowingV()

    function CallgrowingV() {
        setTimeout(function growingV() {
            if (index === 8) {
                callDecreasingV()
            } else {
                ghost.className = 'slideRight'
                let id = `matriz${positionGhost.row[1]}-${positionGhost.column[1] + 1}`
                let nextWay = document.getElementById(id)
                positionGhost.column[1] = positionGhost.column[1] + 1
                checkLooser(nextWay)
                nextWay.appendChild(ghost)

                index++
                setTimeout(growingV, 500)
            }
        }, 500);
    }
    function callDecreasingV() {
        setTimeout(function decreasingV() {
            if (index === 0) {
                CallgrowingV()
            } else {
                ghost.className = 'slideLeft'
                let id = `matriz${positionGhost.row[1]}-${positionGhost.column[1] - 1}`
                let nextWay = document.getElementById(id)
                checkLooser(nextWay)
                positionGhost.column[1] = positionGhost.column[1] - 1

                nextWay.appendChild(ghost)
                index--
                setTimeout(decreasingV, 500)
            }
        }, 500)
    }
}


function ghostHorizontal() {
    let ghost = document.getElementById('ghost1')
    let index = 0

    CallgrowingH()

    function CallgrowingH() {
        setTimeout(function growingH() {
            if (index === 12) {
                callDecreasingH()
            } else {
                ghost.className = 'slideDown'
                let id = `matriz${positionGhost.row[0] + 1}-${positionGhost.column[0]}`
                let nextWay = document.getElementById(id)
                positionGhost.row[0] = positionGhost.row[0] + 1
                checkLooser(nextWay)
                nextWay.appendChild(ghost)

                index++
                setTimeout(growingH, 500)
            }
        }, 500);
    }
    function callDecreasingH() {
        setTimeout(function decreasingH() {
            if (index === 0) {
                CallgrowingH()
            } else {
                ghost.className = 'slideUp'
                let id = `matriz${positionGhost.row[0] - 1}-${positionGhost.column[0]}`
                let nextWay = document.getElementById(id)
                positionGhost.row[0] = positionGhost.row[0] - 1

                checkLooser(nextWay)
                nextWay.appendChild(ghost)
                index--
                setTimeout(decreasingH, 500)
            }
        }, 500)
    }
}
ghostVertical()
ghostHorizontal()

function checkCherry() {
    let compString = `${positionPerson.row}-${positionPerson.column}`
    for (let i = 0; i < positionCherry.length; i++) {
        if (positionCherry[i].join('-') === compString) {
            let cherryRemove = document.getElementById(`cherry${positionPerson.row}`)
            cherryRemove.remove()
            let span = document.getElementById('cont')
            contCherry++
            span.innerText = 'X' + contCherry
        }
    }
}


// Looser
function checkLooser(nextWay) {
    if (nextWay.children[0]) {
        if (nextWay.children[0].id === 'ghost5' || nextWay.children[0].id === 'ghost1') {
            let section = document.querySelector(".invisible2")
            section.classList.remove('invisible2')
        }
        if (nextWay.children[0].id === 'person') {
            let section = document.querySelector(".invisible2")
            section.classList.remove('invisible2')
        }
    }
}

// Winner
function checkWinner(person) {
    if (person.parentNode.classList.contains('finish')) {
        let section = document.querySelector(".invisible")
        section.classList.remove('invisible')
    }
}

let buttonPlay = document.querySelectorAll('.button')
for (let i = 0; i < buttonPlay.length; i++) {
    buttonPlay[i].onclick = function () {
        window.location.reload()
    }
}